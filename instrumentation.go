package acmeserver

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	certExpirationTimes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cert_expiration_time",
			Help: "Certificate expiration time.",
		},
		[]string{"cn"},
	)
	certOk = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cert_ok",
			Help: "Do we have a valid certificate.",
		},
		[]string{"cn"},
	)
	certRenewalCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cert_renewal_total",
			Help: "Total certificate renewal attempts.",
		},
		[]string{"cn"},
	)
	certRenewalErrorCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cert_renewal_errors",
			Help: "Errors while renewing certificates.",
		},
		[]string{"cn"},
	)
)

func init() {
	prometheus.MustRegister(certExpirationTimes, certOk, certRenewalErrorCount)
}

func b2f(b bool) float64 {
	if b {
		return 1
	}
	return 0
}
