package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/acmeserver"
	"gopkg.in/yaml.v3"
)

var (
	addr       = flag.String("addr", ":2780", "tcp `address` to listen on")
	configFile = flag.String("config", "/etc/acme/config.yml", "configuration `file`")
)

// Config ties together the acmeserver Config and the standard
// serverutil HTTP server configuration.
type Config struct {
	ACME   acmeserver.Config        `yaml:",inline"`
	Server *serverutil.ServerConfig `yaml:"http_server"`
}

func loadConfig(path string) (*Config, error) {
	// Read YAML config.
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var config Config
	if err := yaml.NewDecoder(f).Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig(*configFile)
	if err != nil {
		log.Fatal(err)
	}

	var h http.Handler
	var cg acmeserver.CertGenerator
	if !config.ACME.Testing {
		acme, err := acmeserver.NewACME(&config.ACME) // nolint: vetshadow
		if err != nil {
			log.Fatal(err)
		}
		cg = acme
		h = acme.Handler()

	} else {
		// When testing=true there's no content to serve over
		// HTTP. Metrics and other debug pages are added
		// automatically by the serverutil package.
		cg = acmeserver.NewSelfSignedCertGenerator()
		h = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.NotFound(w, r)
		})
	}

	m, err := acmeserver.NewManager(&config.ACME, cg)
	if err != nil {
		log.Fatal(err)
	}

	// Start the acmeserver.Manager in the background. The
	// serverutil package installs SIGTERM handlers that stop the
	// HTTP server, so when it terminates we also kill the context
	// attached to the Manager.
	ctx, cancel := context.WithCancel(context.Background())
	if err := m.Start(ctx); err != nil {
		log.Fatal(err)
	}

	// Set up a SIGHUP handler to reload the configuration.
	hupCh := make(chan os.Signal, 1)
	go func() {
		for {
			<-hupCh
			m.Reload()
		}
	}()
	signal.Notify(hupCh, syscall.SIGHUP)

	if err := serverutil.Serve(h, config.Server, *addr); err != nil {
		log.Fatal(err)
	}

	cancel()
	m.Wait()
}
