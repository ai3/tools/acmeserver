// +build !go1.9

package clientutil

import (
	"context"
	"net"
	"time"
)

// Go < 1.9 does not have net.DialContext, reimplement it in terms of
// net.DialTimeout.
func netDialContext(addr string, connectTimeout time.Duration) func(context.Context, string, string) (net.Conn, error) {
	return func(ctx context.Context, net string, _ string) (net.Conn, error) {
		if deadline, ok := ctx.Deadline(); ok {
			ctxTimeout := time.Until(deadline)
			if ctxTimeout < connectTimeout {
				connectTimeout = ctxTimeout
			}
		}
		return net.DialTimeout(network, addr, connectTimeout)
	}
}
