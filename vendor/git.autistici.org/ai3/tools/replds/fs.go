package replds

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// FS implements the 'storage' interface on the local file system.
type FS struct {
	dir string
}

// newFS returns a new storage rooted at the given directory.
func newFS(dir string) *FS {
	return &FS{dir}
}

// getAllNodes scans the root directory and returns a list of
// Nodes. All errors are ignored.
func (f *FS) getAllNodes() []*Node {
	var nodes []*Node
	dirOffset := len(f.dir) + 1
	if err := filepath.Walk(f.dir, func(path string, info os.FileInfo, err error) error {
		// Ignore errors.
		if err != nil {
			return nil
		}
		// Only look at files.
		if !info.Mode().IsRegular() {
			return nil
		}
		nodes = append(nodes, &Node{
			Path:      path[dirOffset:],
			Timestamp: info.ModTime(),
		})
		return nil
	}); err != nil {
		log.Printf("error scanning directory %s: %v", f.dir, err)
	}
	return nodes
}

// setNode applies a Node to the underlying filesystem.
func (f *FS) setNode(node *Node) error {
	path := filepath.Join(f.dir, node.Path)

	// Tombstone? Then delete the file (ignore errors).
	if node.Deleted {
		return os.Remove(path)
	}

	// Create parent directory if it does not exist.
	dir := filepath.Dir(path)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err = os.MkdirAll(dir, 0755); err != nil {
			return err
		}
	}

	// Dump file contents and reset mtime.
	if err := writeFile(path, node.Value); err != nil {
		return err
	}
	return os.Chtimes(path, node.Timestamp, node.Timestamp)
}

// getNodeValue returns the data of a named node.
func (f *FS) getNodeValue(path string) ([]byte, error) {
	return ioutil.ReadFile(filepath.Join(f.dir, path))
}

// Write a file atomically.
func writeFile(path string, data []byte) error {
	tmp := filepath.Join(filepath.Dir(path), fmt.Sprintf(".%s.tmp", filepath.Base(path)))
	if err := ioutil.WriteFile(tmp, data, 0644); err != nil {
		return err
	}
	err := os.Rename(tmp, path)
	if err != nil {
		// Clean up if something went wrong.
		os.Remove(tmp) // nolint
	}
	return err
}
