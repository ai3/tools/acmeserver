package replds

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"git.autistici.org/ai3/go-common/serverutil"
)

// HTTPServer wraps a Server with an HTTP interface.
type HTTPServer struct {
	*Server
}

// NewHTTPServer creates a new HTTPServer.
func NewHTTPServer(s *Server) *HTTPServer {
	return &HTTPServer{s}
}

func (s *HTTPServer) handleInternalGetNodes(w http.ResponseWriter, r *http.Request) {
	var req internalGetNodesRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := s.internalGetNodes(r.Context(), &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("InternalGetNodes() error: %v", err)
		return
	}

	s.log("internalGetNodes(%+v) -> %+v", nodes2str(req.Nodes), nodes2str(resp.Nodes))

	serverutil.EncodeJSONResponse(w, resp)
}

var emptyJSON = map[string]struct{}{}

func (s *HTTPServer) handleInternalUpdateNodes(w http.ResponseWriter, r *http.Request) {
	var req internalUpdateNodesRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	err := s.internalUpdateNodes(r.Context(), &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("InternalUpdateNodes() error: %v", err)
		return
	}

	s.log("internalUpdateNodes(%+v)", nodes2str(req.Nodes))

	serverutil.EncodeJSONResponse(w, emptyJSON)
}

func (s *HTTPServer) handleSetNodes(w http.ResponseWriter, r *http.Request) {
	var req SetNodesRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := s.setNodes(r.Context(), &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("SetNodes() error: %v", err)
		return
	}

	s.log("setNodes(%+v)", nodes2str(req.Nodes))

	serverutil.EncodeJSONResponse(w, resp)
}

// Handler returns the http.Handler for this server.
func (s *HTTPServer) Handler() http.Handler {
	h := http.NewServeMux()
	h.HandleFunc("/api/internal/update_nodes", s.handleInternalUpdateNodes)
	h.HandleFunc("/api/internal/get_nodes", s.handleInternalGetNodes)
	h.HandleFunc("/api/set_nodes", s.handleSetNodes)
	return h
}

func nodes2str(nodes []*Node) string {
	var tmp []string
	for _, node := range nodes {
		tmp = append(tmp, fmt.Sprintf("%s@%d", node.Path, node.Timestamp.Unix()))
	}
	return strings.Join(tmp, ",")
}
